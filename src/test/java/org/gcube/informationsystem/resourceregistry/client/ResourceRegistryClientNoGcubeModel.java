/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.client;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceRegistryClientNoGcubeModel extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ResourceRegistryClientNoGcubeModel.class);
	
	protected ResourceRegistryClient resourceRegistryClient;
	
	public ResourceRegistryClientNoGcubeModel() {
		if(ContextTest.RESOURCE_REGISTRY_URL !=null  && !ContextTest.RESOURCE_REGISTRY_URL.isEmpty()) {
			resourceRegistryClient = new ResourceRegistryClientImpl(ContextTest.RESOURCE_REGISTRY_URL);
		}else {
			resourceRegistryClient = ResourceRegistryClientFactory.create();
		}
		resourceRegistryClient.setIncludeMeta(true);
	}
	
	@Test
	public void testQuery() throws ResourceRegistryException {
		String res = resourceRegistryClient.rawQuery("SELECT FROM V");
		logger.trace(res);
	}
	
	protected List<String> getTypeNames(List<Type> types){
		List<String> list = new ArrayList<String>(); 
		for(Type t : types) { 
			list.add(t.getName());
		} 
		return list;
	}
	
	protected <ME extends ModelElement> void getTypesFromKnowledge(Class<ME> clazz) throws SchemaNotFoundException, ResourceRegistryException {
		List<Type> types = resourceRegistryClient.getType(clazz, true);
		logger.trace("List size {}\n{}", types.size(), getTypeNames(types));
	}
	
	@Test
	public void testTypesKnowledgeExpiring() throws Exception {
		resourceRegistryClient.getModelKnowledge();
		resourceRegistryClient.renewModelKnowledge();
	}
	
	@Test
	public void testModelKnowledge() throws SchemaNotFoundException, ResourceRegistryException {
		AccessType[] modelTypes = AccessType.getModelTypes();
		for(AccessType accessType : modelTypes) {
			getTypesFromKnowledge(accessType.getTypeClass());
		}
	}
	
	
	@Test
	public void testGetInstancesAndUnmashal() throws Exception {
		resourceRegistryClient.setIncludeMeta(true);
		resourceRegistryClient.setOffset(0);
		resourceRegistryClient.setLimit(-1);
		String instances = resourceRegistryClient.getInstances("EService", false);
		logger.info("{}", instances);
		List<Resource> list = ElementMapper.unmarshalList(Resource.class, instances);
		logger.info("{}", list);
	}
	
	
	@Test
	public void testGetInstanceFromStringAndUnmashal() throws Exception {
		String instance = "{\"type\":\"EService\",\"supertypes\":[\"Service\",\"GCubeResource\",\"Resource\"],\"id\":\"f1b0994a-1ad5-4f32-9a0e-59e5c8667456\",\"metadata\":{\"type\":\"Metadata\",\"creationTime\":\"2024-04-15 11:50:15.415 +0200\",\"createdBy\":\"luca.frosini\",\"lastUpdateBy\":\"luca.frosini\",\"lastUpdateTime\":\"2024-04-15 15:25:49.860 +0200\",\"supertypes\":[\"Property\"]},\"consistsOf\":[{\"type\":\"IsIdentifiedBy\",\"supertypes\":[\"ConsistsOf\"],\"id\":\"f9f0d6f7-a6f9-4d56-ac34-0798899694a7\",\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\",\"supertypes\":[\"Property\"]},\"target\":{\"type\":\"SoftwareFacet\",\"supertypes\":[\"Facet\"],\"id\":\"e1d2242d-3fc6-4432-af2a-d406193b20bc\",\"qualifier\":null,\"name\":\"Liferay\",\"description\":\"D4Science Infrastructure Portal container\",\"optional\":false,\"version\":\"6.2.6-0\",\"group\":\"Liferay\"}},{\"type\":\"ConsistsOf\",\"id\":\"24bf44d4-2e6a-4df8-95ab-7e11d2d22d2c\",\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\",\"supertypes\":[\"Property\"]},\"target\":{\"type\":\"AccessPointFacet\",\"supertypes\":[\"Facet\"],\"id\":\"57444f71-16a4-4f72-8905-3fdf5d456547\",\"authorization\":null,\"endpoint\":\"https://services.d4science.org\",\"protocol\":null,\"entryName\":\"JSONWSUser\",\"description\":\"\",\"users\":[{\"password\":{\"type\":\"Encrypted\",\"value\":\"s9kkLvli1L5SADpvdu2GiQeld0XXZGVrzANNngBo9Xo=\",\"supertypes\":[\"Property\"]},\"username\":{\"type\":\"Encrypted\",\"value\":\"mW35KImZ2LBxWvtKhKLs1NDyphJdt0/JTBMu564HJtk=\",\"supertypes\":[\"Property\"]}}]}},{\"type\":\"ConsistsOf\",\"id\":\"0013ffe4-2103-41fd-8e2d-d4cf330da646\",\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\",\"supertypes\":[\"Property\"]},\"target\":{\"type\":\"StateFacet\",\"supertypes\":[\"Facet\"],\"id\":\"050f48d0-3283-4af6-a47a-73fa70b667b1\",\"date\":\"2024-03-01 15:00:00.000 +0100\",\"value\":\"ready\"}},{\"type\":\"ConsistsOf\",\"id\":\"086214a1-43d8-4a35-9654-2b2e5e1298ab\",\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\",\"supertypes\":[\"Property\"]},\"target\":{\"type\":\"EventFacet\",\"supertypes\":[\"Facet\"],\"id\":\"398bfa1c-ddc5-427d-b6c2-1467fc70b324\",\"date\":\"2024-03-01 15:00:00.000 +0100\",\"event\":\"ready\"}}]}";
		Resource r = ElementMapper.unmarshal(Resource.class, instance);
		logger.info("{}", r);
	}
	
	@Test
	public void testGetInstanceAndUnmashal() throws Exception {
		ContextTest.setContextByName("/gcube");
		String instance = resourceRegistryClient.getInstance("EService", UUID.fromString("f1b0994a-1ad5-4f32-9a0e-59e5c8667456"));
		Resource r = ElementMapper.unmarshal(Resource.class, instance);
		logger.info("{}", r);
	}
}
