/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.informationsystem.base.reference.Direction;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.utils.UUIDUtility;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Hosts;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceRegistryClientTestWikiExamples extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ResourceRegistryClientTestWikiExamples.class);
	
	protected ResourceRegistryClient resourceRegistryClient;
	
	public ResourceRegistryClientTestWikiExamples() {
		if(ContextTest.RESOURCE_REGISTRY_URL !=null  && !ContextTest.RESOURCE_REGISTRY_URL.isEmpty()) {
			resourceRegistryClient = new ResourceRegistryClientImpl(ContextTest.RESOURCE_REGISTRY_URL);
		}else {
			resourceRegistryClient = ResourceRegistryClientFactory.create();
		}
	}
	
	@Before
	public void before() throws Exception {
		setContextByName(PARENT_DEFAULT_TEST_SCOPE);
	}
	
	@Test
	public void testExample1() throws ResourceRegistryException {
		List<EService> list = resourceRegistryClient.getFilteredResources(EService.class, IsIdentifiedBy.class, SoftwareFacet.class, true, null);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample1Alt() throws ResourceRegistryException, Exception {
		String jsonString = resourceRegistryClient.getFilteredResources("EService", "IsIdentifiedBy", "SoftwareFacet", true, null);
		List<EService> list = ElementMapper.unmarshalList(EService.class, jsonString);
		logger.debug("{}", list);
	}
	
	@Test
	@Ignore
	public void testExample2() throws Exception {
		UUID uuid = UUIDUtility.fromString("97984812-90e6-4eb7-b804-50a9ad3fe4fb");
		List<EService> list = resourceRegistryClient.getResourcesFromReferenceFacet(EService.class, IsIdentifiedBy.class, SoftwareFacet.class, uuid, true);
		logger.debug("{}", list);
	}
	
	@Test
	@Ignore
	public void testExample2Alt() throws ResourceRegistryException, Exception{
		SoftwareFacet softwareFacetInstance = new SoftwareFacetImpl();
		UUID uuid = UUIDUtility.fromString("97984812-90e6-4eb7-b804-50a9ad3fe4fb");
		softwareFacetInstance.setID(uuid);
		List<EService> list = resourceRegistryClient.getResourcesFromReferenceFacet(EService.class, IsIdentifiedBy.class, softwareFacetInstance, true);
		logger.debug("{}", list);
	}
	
	@Test
	@Ignore
	public void testExample2Alt2() throws ResourceRegistryException, Exception{
		UUID uuid = UUIDUtility.fromString("97984812-90e6-4eb7-b804-50a9ad3fe4fb");
		String jsonString = resourceRegistryClient.getResourcesFromReferenceFacet("EService", "IsIdentifiedBy", "SoftwareFacet", uuid, true);
		List<EService> list = ElementMapper.unmarshalList(EService.class, jsonString);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample3() throws ResourceRegistryException, Exception{
		Map<String, String> facetConstraints = new HashMap<>();
		facetConstraints.put("group", "VREManagement");
		facetConstraints.put("name", "WhnManager");
		
		List<EService> list = resourceRegistryClient.getFilteredResources(EService.class, IsIdentifiedBy.class, SoftwareFacet.class, true, facetConstraints);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample3Alt() throws ResourceRegistryException, Exception{
		Map<String, String> facetConstraints = new HashMap<>();
		facetConstraints.put("group", "VREManagement");
		facetConstraints.put("name", "WhnManager");
		
		String jsonString = resourceRegistryClient.getFilteredResources("EService", "IsIdentifiedBy", "SoftwareFacet", true, facetConstraints);
		List<EService> list = ElementMapper.unmarshalList(EService.class, jsonString);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample4() throws ResourceRegistryException {
		List<Resource> list = resourceRegistryClient.getFilteredResources(Resource.class, IsIdentifiedBy.class, ContactFacet.class, true, null);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample4Alt() throws ResourceRegistryException, Exception {
		String jsonString = resourceRegistryClient.getFilteredResources("Resource", "IsIdentifiedBy", "ContactFacet", true, null);
		List<Resource> list = ElementMapper.unmarshalList(Resource.class, jsonString);
		logger.debug("{}", list);
	}
	
	
	@Test
	public void testExample5() throws ResourceRegistryException {
		List<Resource> list = resourceRegistryClient.getFilteredResources(Resource.class, ConsistsOf.class, ContactFacet.class, true, null);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample5Alt() throws ResourceRegistryException, Exception {
		String jsonString = resourceRegistryClient.getFilteredResources("Resource", "ConsistsOf", "ContactFacet", true, null);
		List<Resource> list = ElementMapper.unmarshalList(Resource.class, jsonString);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample6() throws ResourceRegistryException, Exception{
		Map<String, String> facetConstraints = new HashMap<>();
		facetConstraints.put("name", "Luca");
		facetConstraints.put("surname", "Frosini");
		
		List<Resource> list = resourceRegistryClient.getFilteredResources(Resource.class, ConsistsOf.class, ContactFacet.class, true, facetConstraints);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample6Alt() throws ResourceRegistryException, Exception{
		Map<String, String> facetConstraints = new HashMap<>();
		facetConstraints.put("name", "Luca");
		facetConstraints.put("surname", "Frosini");
		
		String jsonString = resourceRegistryClient.getFilteredResources("Resource", "ConsistsOf", "ContactFacet", true, facetConstraints);
		List<Resource> list = ElementMapper.unmarshalList(Resource.class, jsonString);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample7() throws ResourceRegistryException {
		List<EService> list = resourceRegistryClient.getRelatedResources(EService.class, Hosts.class, HostingNode.class, Direction.IN, true);
		logger.debug("{}", list);
	}
	
	@Test
	public void testExample7Alt() throws Exception {
		String jsonString = resourceRegistryClient.getRelatedResources("EService", "Hosts", "HostingNode", Direction.IN, true);
		List<EService> list = ElementMapper.unmarshalList(EService.class, jsonString);
		logger.debug("{}", list);
	}
	
	@Test
	@Ignore
	public void testExample8() throws Exception {
		UUID uuid = UUIDUtility.fromString("16032d09-3823-444e-a1ff-a67de4f350a");
		List<EService> list = resourceRegistryClient.getRelatedResourcesFromReferenceResource(EService.class, Hosts.class, HostingNode.class, uuid, Direction.IN, true);
		logger.debug("{}", list);
	}
	
	@Test
	@Ignore
	public void testExample8Alt() throws Exception{
		HostingNode hostingNodeInstance = new HostingNodeImpl();
		UUID uuid = UUIDUtility.fromString("16032d09-3823-444e-a1ff-a67de4f350a");
		hostingNodeInstance.setID(uuid);
		List<EService> list = resourceRegistryClient.getRelatedResourcesFromReferenceResource(EService.class, Hosts.class, hostingNodeInstance, Direction.IN, true);
		logger.debug("{}", list);
	}
	
	@Test
	@Ignore
	public void testExample8Alt2() throws Exception{
		UUID uuid = UUIDUtility.fromString("16032d09-3823-444e-a1ff-a67de4f350a");
		String jsonString = resourceRegistryClient.getRelatedResourcesFromReferenceResource("EService", "Hosts", "HostingNode", uuid, Direction.IN, true);
		List<EService> list = ElementMapper.unmarshalList(EService.class, jsonString);
		logger.debug("{}", list);
	}
	
	@Test
	public void rawQueryExample1() throws ResourceRegistryException, Exception{
		String jsonString = resourceRegistryClient.rawQuery("SELECT FROM SoftwareFacet", true);
		logger.debug("{}", jsonString);
	}
	
	@Test
	public void rawQueryExample2() throws ResourceRegistryException, Exception{
		String jsonString = resourceRegistryClient.rawQuery("SELECT FROM SoftwareFacet");
		List<SoftwareFacet> list = ElementMapper.unmarshalList(SoftwareFacet.class, jsonString);
		logger.debug("{}", list);
	}
	
	@Test
	public void jsonQueryExample1() throws ResourceRegistryException, Exception{
		String jsonQuery = "{\n"
				+ "	\"" + Element.TYPE_PROPERTY + "\": \"EService\",\n"
				+ "	\"consistsOf\": [\n"
				+ "		{\n"
				+ "			\"" + Element.TYPE_PROPERTY + "\": \"IsIdentifiedBy\",\n"
				+ "			\"target\": {\n"
				+ "				\"" + Element.TYPE_PROPERTY + "\": \"SoftwareFacet\",\n"
				+ "				\"group\": \"DataTransfer\",\n"
				+ "				\"name\": \"data-transfer-service\"\n"
				+ "			}\n"
				+ "		},\n"
				+ "		{\n"
				+ "			\"" + Element.TYPE_PROPERTY + "\": \"ConsistsOf\",\n"
				+ "			\"target\": {\n"
				+ "				\"" + Element.TYPE_PROPERTY + "\": \"StateFacet\",\n"
				+ "				\"value\": \"down\"\n"
				+ "			}\n"
				+ "		}\n"
				+ " \n"
				+ "	]\n"
				+ "}";
		
		String jsonString = resourceRegistryClient.jsonQuery(jsonQuery);
		List<EService> list = ElementMapper.unmarshalList(EService.class, jsonString);
		logger.debug("{}", list);
	}
}
