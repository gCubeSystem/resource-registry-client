/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Direction;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.model.impl.properties.MetadataImpl;
import org.gcube.informationsystem.model.knowledge.ModelKnowledge;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.tree.Tree;
import org.gcube.informationsystem.types.knowledge.TypeInformation;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.utils.UUIDManager;
import org.gcube.informationsystem.utils.UUIDUtility;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualService;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceRegistryClientTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ResourceRegistryClientTest.class);
	
	protected ResourceRegistryClient resourceRegistryClient;
	
	public ResourceRegistryClientTest() {
		if(ContextTest.RESOURCE_REGISTRY_URL !=null  && !ContextTest.RESOURCE_REGISTRY_URL.isEmpty()) {
			resourceRegistryClient = new ResourceRegistryClientImpl(ContextTest.RESOURCE_REGISTRY_URL);
		}else {
			resourceRegistryClient = ResourceRegistryClientFactory.create();
		}
		resourceRegistryClient.setIncludeMeta(true);
	}
	
	@Test
	public void testQuery() throws ResourceRegistryException {
		String res = resourceRegistryClient.rawQuery("SELECT FROM V");
		logger.trace(res);
	}
	
	protected List<String> getTypeNames(List<Type> types){
		List<String> list = new ArrayList<String>(); 
		for(Type t : types) { 
			list.add(t.getName());
		} 
		return list;
	}
	
	protected <ME extends ModelElement> void getTypesFromKnowledge(Class<ME> clazz) throws SchemaNotFoundException, ResourceRegistryException {
		List<Type> types = resourceRegistryClient.getType(clazz, true);
		logger.trace("List size {}\n{}", types.size(), getTypeNames(types));
	}
	
	@Test
	public void testTypesKnowledgeExpiring() throws Exception {
		resourceRegistryClient.getModelKnowledge();
		resourceRegistryClient.renewModelKnowledge();
	}
	
	@Test
	public void testModelKnowledge() throws SchemaNotFoundException, ResourceRegistryException {
		AccessType[] modelTypes = AccessType.getModelTypes();
		for(AccessType accessType : modelTypes) {
			getTypesFromKnowledge(accessType.getTypeClass());
		}
	}
	
	@Test
	public void testGetFacetSchema() throws SchemaNotFoundException, ResourceRegistryException {
		List<Type> types = resourceRegistryClient.getType(ContactFacet.class, true);
		logger.trace("List size {}\n{}", types.size(), getTypeNames(types));
	}
	
	@Test
	public void testGetResourceSchema() throws SchemaNotFoundException, ResourceRegistryException {
		List<Type> types = resourceRegistryClient.getType(HostingNode.class, true);
		logger.trace("List size {}\n{}", types.size(), getTypeNames(types));
		types = resourceRegistryClient.getType(VirtualService.class, true);
		logger.trace("List size {}\n{}", types.size(), getTypeNames(types));
		types = resourceRegistryClient.getType(Service.class, true);
		logger.trace("List size {}\n{}", types.size(), getTypeNames(types));
	}
	
	@Test
	public void testGetResourceWithLevel() throws SchemaNotFoundException, ResourceRegistryException {
		for(int i=0; i<5; i++) {
			List<Type> types = resourceRegistryClient.getType(Resource.class, i);
			logger.trace("List size with level {} is {}\n{}", i, types.size(), getTypeNames(types));
		}
	}
	
	@Test
	public void testGetModelKnowledge() throws SchemaNotFoundException, ResourceRegistryException {
		AccessType[] modelTypes = AccessType.getModelTypes();
		ModelKnowledge<Type, TypeInformation> modelKnowledge = ((ResourceRegistryClientImpl) resourceRegistryClient).getModelKnowledge();
		for(AccessType accessType : modelTypes) {
			Tree<Type> tree = modelKnowledge.getTree(accessType);
			logger.trace("{}", tree);
		}
	}
	
	@Test
	public void testGetResourceTree() throws SchemaNotFoundException, ResourceRegistryException {
		Tree<Type> tree = ((ResourceRegistryClientImpl) resourceRegistryClient).getModelKnowledge().getTree(AccessType.RESOURCE);
		logger.trace("{}", tree);
		
		
	}
	
	@Test
	public void testGetNode() throws SchemaNotFoundException, ResourceRegistryException {
		AccessType[] modelTypes = AccessType.getModelTypes();
		for(AccessType accessType : modelTypes) {
			Node<Type> node = resourceRegistryClient.getTypeTreeNode(accessType.getTypeClass());
			logger.trace("{}", node);
		}
	}
	
	interface Aux extends Service {
		
	}
	
	@Test(expected = SchemaNotFoundException.class)
	public void testException() throws SchemaNotFoundException, ResourceRegistryException {
		resourceRegistryClient.getType("Aux", true);
	}
	
	/* The following tests are commented because we need to create the instances for tests. this is done in
	 * resource registry publisher, which uses client APis to test the publishing as weel as this client.
	 */
	
	// @Test
	public void testExists() throws ResourceRegistryException {
		UUID uuid = UUID.fromString("bdaccb35-7f27-45a6-8ca9-11d467cb9233");
		resourceRegistryClient.existInstance(EService.NAME, uuid);
	}
	
	// @Test
	public void testExistsByClass() throws Exception {
		UUID uuid = UUIDUtility.fromString("bdaccb35-7f27-45a6-8ca9-11d467cb9233");
		resourceRegistryClient.existInstance(EService.class, uuid);
	}
	
	// @Test
	public void testGetInstance() throws Exception {
		UUID uuid = UUIDUtility.fromString("bdaccb35-7f27-45a6-8ca9-11d467cb9233");
		String eService = resourceRegistryClient.getInstance(EService.NAME, uuid);
		logger.trace("{}", eService);
	}
	
	// @Test
	public void testGetInstanceByClass() throws Exception {
		UUID uuid = UUIDUtility.fromString("bdaccb35-7f27-45a6-8ca9-11d467cb9233");
		EService eService = resourceRegistryClient.getInstance(EService.class, uuid);
		logger.trace("{}", eService);
	}
	
	@Test
	public void testGetInstances() throws ResourceRegistryException {
		String eServices = resourceRegistryClient.getInstances(EService.NAME, true);
		logger.trace("{}", eServices);
	}
	
	@Test
	public void testGetInstancesByClass() throws Exception {
		List<EventFacet> eventFacets = resourceRegistryClient.getInstances(EventFacet.class, true);
		logger.trace("{}", eventFacets);
		eventFacets = ElementMapper.unmarshalList(EventFacet.class, eventFacets.toString());
		logger.trace("{}", eventFacets);
	}
	
	@Test
	public void testGetRelatedResourcesByClasses() throws ResourceRegistryException {
		List<EService> eServices = resourceRegistryClient.getRelatedResources(EService.class, IsRelatedTo.class,
				Resource.class, Direction.OUT, true);
		logger.trace("{}", eServices);
	}
	
	// @Test
	public void testGetRelatedResourcesFromReferenceResourceByClasses() throws Exception {
		UUID uuid = UUIDUtility.fromString("b0d15e45-62af-4221-b785-7d014f10e631");
		HostingNode hostingNode = new HostingNodeImpl();
		hostingNode.setID(uuid);
		Metadata metadata = new MetadataImpl();
		hostingNode.setMetadata(metadata);
		List<EService> eServices = resourceRegistryClient.getRelatedResourcesFromReferenceResource(EService.class,
				IsRelatedTo.class, hostingNode, Direction.OUT, true);
		logger.trace("{}", eServices);
	}
	
	@Test
	public void testGetFilteredResourcesByClasses() throws ResourceRegistryException, JsonProcessingException {
		Map<String,String> map = new HashMap<>();
		map.put("group", "VREManagement");
		map.put("name", "SmartExecutor");
		List<EService> eServices = resourceRegistryClient.getFilteredResources(EService.class, IsIdentifiedBy.class,
				SoftwareFacet.class, true, map);
		for(EService eService : eServices) {
			logger.trace("{}", ElementMapper.marshal(eService));
		}
	}
	
	
	// @Test
	public void testGetResourcesFromReferenceFacet() throws ResourceRegistryException, JsonProcessingException {
		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setID(UUIDManager.getInstance().generateValidUUID());
		Metadata metadata = new MetadataImpl();
		softwareFacet.setMetadata(metadata);
		List<EService> eServices = resourceRegistryClient.getResourcesFromReferenceFacet(EService.class, IsIdentifiedBy.class, softwareFacet, true);
		for(EService eService : eServices) {
			logger.trace("{}", ElementMapper.marshal(eService));
		}
	}
	
	@Test
	public void testGetAllContexts() throws Exception {
		List<Context> contexts = resourceRegistryClient.getAllContext();
		logger.debug("{}", contexts);
		
		ContextCache contextCache = ContextCache.getInstance();
		Map<UUID, String> uuidToContextFullName = contextCache.getUUIDToContextFullNameAssociation();
		logger.debug("{}", uuidToContextFullName);

		
		for(Context c : contexts) {
			UUID uuid = c.getID();
			if(c.getParent()!=null) {
				IsParentOf isParentOf = c.getParent();
				Context parentContext = isParentOf.getSource();
				UUID parentUUID = parentContext.getID();
				Assert.assertEquals(parentContext, contextCache.getContextByUUID(parentUUID));
				List<IsParentOf> children = parentContext.getChildren();
				boolean found = false;
				for(IsParentOf ipo : children) {
					if(ipo.equals(isParentOf)) {
						found = true;
						break;
					}
				}
				Assert.assertTrue(found);
				logger.debug("{} : {} (parent {} : {})", c.getID(), contextCache.getContextFullNameByUUID(uuid), parentUUID, contextCache.getContextFullNameByUUID(parentUUID));
			}else {
				logger.debug("{} : {}", c.getID(), contextCache.getContextFullNameByUUID(uuid));
			}
		}
		
		Context currentContext = resourceRegistryClient.getCurrentContext();
		logger.debug("Current context : {}", currentContext);
		
		for(Context c : contexts) {
			UUID uuid = c.getID();
			Context context = resourceRegistryClient.getContext(uuid);
			String fullName = ContextCache.getInstance().getContextFullNameByUUID(uuid);
			logger.debug("{} - {} : {}", uuid, fullName, context);
		}
	}
	
	@Test
	public void testParameters() throws Exception {
		resourceRegistryClient.setLimit(1);
		resourceRegistryClient.setIncludeMeta(true);
		List<Context> contexts = ((ResourceRegistryClientImpl) resourceRegistryClient).getAllContextFromServer();
		Assert.assertTrue(contexts.size()==1);
		Assert.assertTrue(contexts.get(0).getMetadata()!=null);
		
		resourceRegistryClient.setLimit(1);
		resourceRegistryClient.setIncludeMeta(false);
		contexts = ((ResourceRegistryClientImpl) resourceRegistryClient).getAllContextFromServer();
		Assert.assertTrue(contexts.size()==1);
		Assert.assertTrue(contexts.get(0).getMetadata()==null);
	}
	
	
	@Test
	public void testRunQueryTemplates() throws Exception {
		String ret = resourceRegistryClient.runQueryTemplate("IS-Monitoring-All-HostingNode");
		logger.trace(ret);
		ret = resourceRegistryClient.runQueryTemplate("IS-Monitoring-All-EService");
		logger.trace(ret);
		ret = resourceRegistryClient.runQueryTemplate("IS-Monitoring-All-HostingNode", "{}");
		logger.trace(ret);
		ret = resourceRegistryClient.runQueryTemplate("IS-Monitoring-All-EService", "{}");
		logger.trace(ret);
	}
	
}
