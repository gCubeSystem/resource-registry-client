package org.gcube.informationsystem.resourceregistry.client;

import java.util.Collection;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.knowledge.TypesDiscoverer;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.types.reference.Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class RRCTypesDiscoverer implements TypesDiscoverer<Type> {

	protected ResourceRegistryClientImpl resourceRegistryClient;
	
	public RRCTypesDiscoverer(ResourceRegistryClientImpl resourceRegistryClient) {
		this.resourceRegistryClient = resourceRegistryClient;
	}
	
	@Override
	public Collection<Type> discover(AccessType accessType) {
		try {
			return resourceRegistryClient.getTypeFromServer(accessType.getTypeClass(), true);
		} catch (ResourceRegistryException e) {
			throw new RuntimeException(e);
		}
	}

}
