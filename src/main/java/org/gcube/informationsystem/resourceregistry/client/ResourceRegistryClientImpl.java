package org.gcube.informationsystem.resourceregistry.client;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.gxhttp.reference.GXConnection;
import org.gcube.common.gxhttp.request.GXHTTPStringRequest;
import org.gcube.common.http.GXHTTPUtility;
import org.gcube.informationsystem.base.reference.Direction;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.knowledge.ModelKnowledge;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCacheRenewal;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates.QueryTemplateNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.request.BaseRequestInfo;
import org.gcube.informationsystem.resourceregistry.api.rest.AccessPath;
import org.gcube.informationsystem.resourceregistry.api.rest.ContextPath;
import org.gcube.informationsystem.resourceregistry.api.rest.InstancePath;
import org.gcube.informationsystem.resourceregistry.api.rest.QueryTemplatePath;
import org.gcube.informationsystem.resourceregistry.api.rest.TypePath;
import org.gcube.informationsystem.resourceregistry.api.rest.httputils.HTTPUtility;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.knowledge.TypeInformation;
import org.gcube.informationsystem.types.knowledge.TypesKnowledge;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.utils.TypeUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceRegistryClientImpl extends BaseRequestInfo implements ResourceRegistryClient {
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceRegistryClientImpl.class);
	
	private static final String ACCEPT_HTTP_HEADER_KEY = "Accept";
	private static final String CONTENT_TYPE_HTTP_HEADER_KEY = "Content-Type";
	
	protected final String address;
	
	protected Map<String, String> headers;
	
	protected ContextCache contextCache;
	
	protected TypesKnowledge typesKnowledge;
	
	@Deprecated
	@Override
	public boolean isIncludeContextsInHeader() {
		return includeContexts();
	}

	@Deprecated
	@Override
	public void setIncludeContextsInHeader(boolean includeContexts) {
		setIncludeContexts(includeContexts);
	}
	
	private void addOptionalQueryParameters(Map<String,String> queryParams) throws UnsupportedEncodingException {
		addHierarchicalMode(queryParams);
		addIncludeContexts(queryParams);
		addIncludeMeta(queryParams);
		addIncludeAllMeta(queryParams);
	}
	
	private GXHTTPStringRequest includeAdditionalQueryParameters(GXHTTPStringRequest gxHTTPStringRequest) throws UnsupportedEncodingException{
		Map<String,String> queryParams = new HashMap<>();
		return includeAdditionalQueryParameters(gxHTTPStringRequest, queryParams);
	}
	
	private GXHTTPStringRequest includeAdditionalQueryParameters(GXHTTPStringRequest gxHTTPStringRequest, Map<String,String> queryParams) throws UnsupportedEncodingException{
		if(queryParams==null) {
			queryParams = new HashMap<>();
		}
		addOptionalQueryParameters(queryParams);
		return gxHTTPStringRequest.queryParams(queryParams);
	}
	
	private void addHierarchicalMode(Map<String,String> queryParams) throws UnsupportedEncodingException{
		if(hierarchicalMode) {
			queryParams.put(AccessPath.HIERARCHICAL_MODE_QUERY_PARAMETER, Boolean.toString(hierarchicalMode));
		}
	}
	
	private void addIncludeContexts(Map<String,String> queryParams) throws UnsupportedEncodingException{
		if(includeContexts) {
			queryParams.put(AccessPath.INCLUDE_CONTEXTS_QUERY_PARAMETER, Boolean.toString(includeContexts));
		}
	}
	
	private void addIncludeMeta(Map<String,String> queryParams) throws UnsupportedEncodingException{
		addIncludeMeta(queryParams, includeMeta);
	}
	
	private void addIncludeMeta(Map<String,String> queryParams, boolean includeMeta) throws UnsupportedEncodingException{
		if(includeMeta) {
			queryParams.put(AccessPath.INCLUDE_META_QUERY_PARAMETER, Boolean.toString(includeMeta));
		}
	}
	
	private void addIncludeAllMeta(Map<String,String> queryParams) throws UnsupportedEncodingException{
		if(allMeta) {
			queryParams.put(AccessPath.INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER, Boolean.toString(allMeta));
		}
	}
	
	private void addOffset(Map<String,String> queryParams) throws UnsupportedEncodingException{
		addOffset(queryParams, offset);
	}
	
	private void addOffset(Map<String,String> queryParams, Integer offset) throws UnsupportedEncodingException{
		if(offset!=null) {
			queryParams.put(AccessPath.OFFSET_QUERY_PARAMETER, offset.toString());
		}
	}
	
	private void addLimit(Map<String,String> queryParams) throws UnsupportedEncodingException{
		addLimit(queryParams, limit);
	}
	
	private void addLimit(Map<String,String> queryParams, Integer limit) throws UnsupportedEncodingException{
		if(limit!=null) {
			queryParams.put(AccessPath.LIMIT_QUERY_PARAMETER, limit.toString());
		}
	}
	
	protected ContextCacheRenewal contextCacheRenewal = new ContextCacheRenewal() {
		
		@Override
		public List<Context> renew() throws ResourceRegistryException {
			return getAllContextFromServer(true, 0, BaseRequestInfo.UNBOUNDED_LIMIT);
		}
		
	};
	
	@Override
	public void addHeader(String name, String value) {
		headers.put(name, value);
	}
	
	protected GXHTTPStringRequest getGXHTTPStringRequest() {
		GXHTTPStringRequest gxHTTPStringRequest = GXHTTPUtility.getGXHTTPStringRequest(address);
		gxHTTPStringRequest.from(this.getClass().getSimpleName());
		for(String name : headers.keySet()) {
			gxHTTPStringRequest.header(name, headers.get(name));
		}
		return gxHTTPStringRequest;
	}
	
	public ResourceRegistryClientImpl(String address) {
		this(address, true, true);
	}
	
	public ResourceRegistryClientImpl(String address, boolean sharedContextCache, boolean sharedModelKnowledge) {
		super();
		this.address = address;
		this.headers = new HashMap<>();
		if(sharedContextCache) {
			this.contextCache = ContextCache.getInstance();
		}else {
			this.contextCache = new ContextCache();
		}
		this.contextCache.setContextCacheRenewal(contextCacheRenewal);
		
		if(sharedModelKnowledge) {
			this.typesKnowledge = TypesKnowledge.getInstance();
		}else {
			this.typesKnowledge = new TypesKnowledge();
		}
		typesKnowledge.setTypesDiscoverer(new RRCTypesDiscoverer(this));
	}
	
	/**
	 * It reads all the contexts from server. 
	 * The cache used for contexts is bypassed and not updated.
	 * @return All Contexts read from server
	 * @throws ResourceRegistryException
	 */
	public List<Context> getAllContextFromServer() throws ResourceRegistryException {
		return getAllContextFromServer(includeMeta, offset, limit);
	}
	
	protected List<Context> getAllContextFromServer(boolean includeMeta, Integer offset, Integer limit) throws ResourceRegistryException {
		try {
			logger.info("Going to read all {}s", Context.NAME);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.CONTEXTS_PATH_PART);
			
			Map<String,String> parameters = new HashMap<>();
			addIncludeMeta(parameters, includeMeta);
			addOffset(parameters, offset);
			addLimit(parameters, limit);
			gxHTTPStringRequest.queryParams(parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			String ret = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.debug("Got Contexts are {}", ret);
			return ElementMapper.unmarshalList(Context.class, ret);
			
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while getting {} schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while getting {}schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public List<Context> getAllContext() throws ResourceRegistryException {
		return contextCache.getContexts();
	}
	
	@Override
	public ContextCache getContextCache() {
		return contextCache;
	}
	
	@Override
	public ModelKnowledge<Type, TypeInformation> getModelKnowledge() {
		return typesKnowledge.getModelKnowledge();
	}
	
	@Override
	public void renewModelKnowledge() {
		typesKnowledge.renew();
	}

	/**
	 * It reads the context from server. 
	 * The cache used for contexts is bypassed and not updated.
	 * @param uuid
	 * @return the Contexts read from server
	 * @throws ContextNotFoundException
	 * @throws ResourceRegistryException
	 */
	public Context getContextFromServer(UUID uuid) throws ContextNotFoundException, ResourceRegistryException {
		return getContextFromServer(uuid.toString());
	}
	
	protected Context getContextFromServer(String uuid) throws ContextNotFoundException, ResourceRegistryException {
		try {
			logger.info("Going to get current {} ", Context.NAME);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.CONTEXTS_PATH_PART);
			gxHTTPStringRequest.path(uuid);
			
			Map<String,String> parameters = new HashMap<>();
			addIncludeMeta(parameters);
			gxHTTPStringRequest.queryParams(parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			Context context = HTTPUtility.getResponse(Context.class, httpURLConnection);
			
			logger.debug("Got Context is {}", ElementMapper.marshal(context));
			return context;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while getting {} schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while getting {}schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public boolean existContext(String uuid) throws ResourceRegistryException {
		return existContext(UUID.fromString(uuid));
	}
	
	@Override
	public boolean existContext(UUID uuid) throws ResourceRegistryException {
		try {
			getContext(uuid);
			return true;
		}catch (ContextNotFoundException e) {
			return false;
		}
	}
	
	@Override
	public Context getContext(String uuid) throws ContextNotFoundException, ResourceRegistryException {
		return getContext(UUID.fromString(uuid));
	}
	
	@Override
	public Context getContext(UUID uuid) throws ContextNotFoundException, ResourceRegistryException {
		Context context = contextCache.getContextByUUID(uuid);;
		if(context == null) {
			context = getContextFromServer(ContextPath.CURRENT_CONTEXT_PATH_PART);
			contextCache.cleanCache();
			contextCache.refreshContextsIfNeeded();
			Context c = contextCache.getContextByUUID(context.getID());
			if(c!=null){
				context = c;
			}else {
				logger.error("Context with UUID {} is {}. It is possibile to get it from the server but not from the cache. This is very strange and should not occur.", uuid, context);
			}
		}
		return context;
	}
	
	@Override
	public Context getCurrentContext() throws ContextNotFoundException, ResourceRegistryException {
		String contextFullName = org.gcube.common.context.ContextUtility.getCurrentContextFullName();
		UUID uuid = contextCache.getUUIDByFullName(contextFullName);
		Context context = null;
		if(uuid == null) {
			context = getContextFromServer(ContextPath.CURRENT_CONTEXT_PATH_PART);
			contextCache.cleanCache();
			contextCache.refreshContextsIfNeeded();
			Context c = contextCache.getContextByUUID(context.getID());
			if(c!=null){
				context = c;
			}else {
				logger.error("Current Context is {}. It is possibile to get it from the server but not from the cache. This is very strange and should not occur.", contextFullName);
			}
		}else {
			context = contextCache.getContextByUUID(uuid);
		}
		return context;
	}
	
	@Override
	public <ME extends ModelElement> boolean existType(Class<ME> clazz) throws ResourceRegistryException {
		return existType(TypeUtility.getTypeName(clazz));
	}	
	
	@Override
	public boolean existType(String typeName) throws ResourceRegistryException {
		try {
			return typesKnowledge.getModelKnowledge().getTypeByName(typeName) != null;
		}catch (RuntimeException e) {
			return false;
		}
	}
	
	public boolean existTypeFromServer(String typeName) throws ResourceRegistryException {
		try {
			logger.info("Going to get {} schema", typeName);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.TYPES_PATH_PART);
			gxHTTPStringRequest.path(typeName);
			
			Map<String,String> parameters = new HashMap<>();
			parameters.put(InstancePath.POLYMORPHIC_QUERY_PARAMETER, Boolean.FALSE.toString());
			gxHTTPStringRequest.queryParams(parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.head();
			HTTPUtility.getResponse(String.class, httpURLConnection);

			return true;
		} catch (NotFoundException e) {
			return false;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while getting {} schema for {}", polymorphic ? AccessPath.POLYMORPHIC_PARAM + " " : "",
			//		type, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while getting {}schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw new ResourceRegistryException(e);
		}
	}
	
	public <ERElem extends ERElement> List<Type> getTypeFromTypesKnowledge(String typeName, Boolean polymorphic)
			throws SchemaNotFoundException, ResourceRegistryException {
		return getTypeFromTypesKnowledge(typeName, polymorphic, -1);
	}
	
	
	public <ERElem extends ERElement> List<Type> getTypeFromTypesKnowledge(String typeName, int level)
			throws SchemaNotFoundException, ResourceRegistryException {
		return getTypeFromTypesKnowledge(typeName, true, level);
	}
	
	protected List<Type> addChildren(Node<Type> node, List<Type> types, int currentLevel, int maxLevel) {
		if(maxLevel>=0 && maxLevel <= currentLevel) {
			return types;
		}
		
		Set<Node<Type>> children = node.getChildrenNodes();
		if(children!=null && children.size()>0) {
			for(Node<Type> child : children) {
				types.add(child.getNodeElement());
				types = addChildren(child, types, ++currentLevel, maxLevel);
			}
		}
		
		return types;
	}
	
	public <ERElem extends ERElement> List<Type> getTypeFromTypesKnowledge(String typeName, Boolean polymorphic, int level)
			throws SchemaNotFoundException, ResourceRegistryException {
		
		Node<Type> node = getTypeTreeNode(typeName);
		
		List<Type> types = new ArrayList<>();
		types.add(node.getNodeElement());
		
		if (polymorphic) {
			addChildren(node, types, 0, level);
		} 
		
		return types;
	}
	
	@Override
	public String getType(String typeName, Boolean polymorphic) throws SchemaNotFoundException, ResourceRegistryException {
		try { 
			List<Type> types = getTypeFromTypesKnowledge(typeName, polymorphic);
			return TypeMapper.serializeTypeDefinitions(types);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}

	@Override
	public String getType(String typeName, int level) throws SchemaNotFoundException, ResourceRegistryException {
		try { 
			List<Type> types = getTypeFromTypesKnowledge(typeName, level);
			return TypeMapper.serializeTypeDefinitions(types);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	@Override
	public <ME extends ModelElement> List<Type> getType(Class<ME> clazz, Boolean polymorphic)
			throws SchemaNotFoundException, ResourceRegistryException {
		try {
			String typeName = TypeUtility.getTypeName(clazz);
			return getTypeFromTypesKnowledge(typeName, polymorphic);
		} catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}

	@Override
	public <ME extends ModelElement> List<Type> getType(Class<ME> clazz, int level)
			throws SchemaNotFoundException, ResourceRegistryException {
		try {
			String typeName = TypeUtility.getTypeName(clazz);
			return getTypeFromTypesKnowledge(typeName, level);
		} catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	@Override
	public Node<Type> getTypeTreeNode(String typeName) throws SchemaNotFoundException, ResourceRegistryException {
		try { 
			Node<Type> node = null;
			try {
				node = typesKnowledge.getModelKnowledge().getNodeByName(typeName);
			} catch (RuntimeException e) {
				throw new SchemaNotFoundException(e);
			}
			return node;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}

	@Override
	public <ME extends ModelElement> Node<Type> getTypeTreeNode(Class<ME> clazz)
			throws SchemaNotFoundException, ResourceRegistryException {
		try {
			String typeName = TypeUtility.getTypeName(clazz);
			return getTypeTreeNode(typeName);
		} catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	public <ME extends ModelElement> List<Type> getTypeFromServer(Class<ME> clazz, Boolean polymorphic)
			throws SchemaNotFoundException, ResourceRegistryException {
		try {
			String json = getTypeFromServer(TypeUtility.getTypeName(clazz), polymorphic);
			return TypeMapper.deserializeTypeDefinitions(json);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	public String getTypeFromServer(String typeName, Boolean polymorphic)
			throws SchemaNotFoundException, ResourceRegistryException {
		try {
			logger.info("Going to get {} schema", typeName);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.TYPES_PATH_PART);
			gxHTTPStringRequest.path(typeName);
			
			Map<String,String> parameters = new HashMap<>();
			addIncludeMeta(parameters);
			parameters.put(TypePath.POLYMORPHIC_QUERY_PARAMETER, polymorphic.toString());
			gxHTTPStringRequest.queryParams(parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			String json = HTTPUtility.getResponse(String.class, httpURLConnection);

			logger.debug("Got schema for {} is {}", typeName, json);
			return json;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while getting {} schema for {}", polymorphic ? AccessPath.POLYMORPHIC_PARAM + " " : "",
			//		type, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while getting {}schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <ERElem extends ERElement> List<ERElem> getInstances(Class<ERElem> clazz, Boolean polymorphic)
			throws ResourceRegistryException {
		String type = TypeUtility.getTypeName(clazz);
		String ret = getInstances(type, polymorphic);
		try {
			return (List<ERElem>) ElementMapper.unmarshalList(ERElement.class, ret);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String getInstances(String type, Boolean polymorphic) throws ResourceRegistryException {
		try {
			logger.info("Going to get all instances of {} ", type);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			
			Map<String,String> parameters = new HashMap<>();
			addOffset(parameters);
			addLimit(parameters);
			parameters.put(InstancePath.POLYMORPHIC_QUERY_PARAMETER, polymorphic.toString());
			includeAdditionalQueryParameters(gxHTTPStringRequest, parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			
			String ret = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.debug("Got instances of {} are {}", type, ret);
			return ret;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while getting {} instances", type, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while getting {} instances", type, e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public <ERElem extends ERElement> boolean existInstance(Class<ERElem> clazz, UUID uuid)
			throws AvailableInAnotherContextException, ResourceRegistryException {
		String type = TypeUtility.getTypeName(clazz);
		return existInstance(type, uuid);
	}
	
	@Override
	public boolean existInstance(String type, UUID uuid)
			throws AvailableInAnotherContextException, ResourceRegistryException {
		try {
			logger.info("Going to check if {} with UUID {} exists", type, uuid);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(uuid.toString());
			
			Map<String,String> queryParams =  new HashMap<>();
			addHierarchicalMode(queryParams);
			gxHTTPStringRequest.queryParams(queryParams);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.head();
			HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.debug("{} with UUID {} exists", type, uuid);
			return true;
		} catch (NotFoundException e) {
			return false;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while checking if {} with UUID {} exists.", type, uuid,
			// e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while checking if {} with UUID {} exists.", type, uuid,
			// e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public <ERElem extends ERElement> ERElem getInstance(Class<ERElem> clazz, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		String type = TypeUtility.getTypeName(clazz);
		String ret = getInstance(type, uuid);
		try {
			return ElementMapper.unmarshal(clazz, ret);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String getInstance(String type, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		try {
			logger.info("Going to get {} with UUID {}", type, uuid);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(uuid.toString());
			
			includeAdditionalQueryParameters(gxHTTPStringRequest);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			String ret = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.debug("Got {} with UUID {} is {}", type, uuid, ret);
			return ret;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while getting {} with UUID {}", type, uuid, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while getting {} with UUID {}", type, uuid, e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String rawQuery(String query)
			throws InvalidQueryException, ResourceRegistryException {
		return rawQuery(query, false);
	}
	
	@Override
	public String rawQuery(String query, boolean raw)
			throws InvalidQueryException, ResourceRegistryException {
		
		try {
			logger.info("Going to query. {}", query);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.QUERY_PATH_PART);
			
			Map<String,String> parameters = new HashMap<>();
			parameters.put(AccessPath.Q_QUERY_PARAMETER, query);
			parameters.put(AccessPath.RAW_QUERY_PARAMETER, Boolean.toString(raw));
			
			if(raw) {
				addHierarchicalMode(parameters);
				gxHTTPStringRequest.queryParams(parameters);
			} else {
				includeAdditionalQueryParameters(gxHTTPStringRequest, parameters);
			}
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();

			String ret = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.trace("Query result is {}", ret);
			return ret;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while querying", e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while querying", e);
			throw new RuntimeException(e);
		}
	}
	
	protected String getRelated(String entityType, String relationType, String referenceEntityType,
			UUID referenceEntity, Direction direction, Boolean polymorphic, Map<String,String> facetConstraints)
			throws ResourceRegistryException {
		
		try {
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.QUERY_PATH_PART);
			gxHTTPStringRequest.path(entityType);
			gxHTTPStringRequest.path(relationType);
			gxHTTPStringRequest.path(referenceEntityType);
			
			Map<String,String> parameters = new HashMap<>();
			parameters.put(AccessPath._DIRECTION_QUERY_PARAMETER, direction.name());
			parameters.put(AccessPath._POLYMORPHIC_QUERY_PARAMETER, polymorphic.toString());
			
			if(referenceEntity == null) {
				if(facetConstraints != null && facetConstraints.size() > 0) {
					logger.info("Going to get {} linked by a {} Relation to a {} having {}", entityType, relationType,
							referenceEntityType, facetConstraints);
					parameters.putAll(facetConstraints);
				} else {
					logger.info("Going to get {} linked by a {} Relation to a {}", entityType, relationType,
							referenceEntityType);
				}
			} else {
				logger.info("Going to get {} linked by {} to {} with UUID {}", entityType, relationType,
						referenceEntityType, referenceEntity);
				parameters.put(AccessPath._REFERENCE_QUERY_PARAMETER, referenceEntity.toString());
			}
			
			includeAdditionalQueryParameters(gxHTTPStringRequest, parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();

			String json = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			if(referenceEntity == null) {
				logger.info("{} linked by {} to/from {} having {} are {}", entityType, relationType,
						referenceEntityType, facetConstraints, json);
				
			} else {
				logger.info("{} linked by {} to/from {} with UUID {} are", entityType, relationType,
						referenceEntityType, referenceEntity, json);
			}
			
			return json;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	@Override
	public <R extends Resource, C extends ConsistsOf<?,?>, F extends Facet> List<R> getResourcesFromReferenceFacet(
			Class<R> resourceClass, Class<C> consistsOfClass, F referenceFacet,
			boolean polymorphic) throws ResourceRegistryException {
		UUID referenceFacetUUID = referenceFacet.getID();
		@SuppressWarnings("unchecked")
		Class<F> facetClass = (Class<F>) referenceFacet.getClass();
		return getResourcesFromReferenceFacet(resourceClass, consistsOfClass, facetClass, referenceFacetUUID,
				polymorphic);
	}
	
	@SuppressWarnings("unchecked")
	public <R extends Resource, C extends ConsistsOf<?,?>, F extends Facet> List<R> getResourcesFromReferenceFacet(
			Class<R> resourceClass, Class<C> consistsOfClass, Class<F> facetClass, UUID referenceFacetUUID,
			boolean polymorphic) throws ResourceRegistryException {
		String resourceType = TypeUtility.getTypeName(resourceClass);
		String consistsOfType = TypeUtility.getTypeName(consistsOfClass);
		String facetType = TypeUtility.getTypeName(facetClass);
		String ret = getResourcesFromReferenceFacet(resourceType, consistsOfType, facetType, referenceFacetUUID,
				polymorphic);
		try {
			return (List<R>) ElementMapper.unmarshalList(Resource.class, ret);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String getResourcesFromReferenceFacet(String resourceType, String consistsOfType, String facetType,
			UUID facetUUID, boolean polymorphic) throws ResourceRegistryException {
		return getRelated(resourceType, consistsOfType, facetType, facetUUID, Direction.OUT, polymorphic);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <R extends Resource, C extends ConsistsOf<?,?>, F extends Facet> List<R> getFilteredResources(
			Class<R> resourceClass, Class<C> consistsOfClass, Class<F> facetClass, boolean polymorphic,
			Map<String,String> facetConstraints) throws ResourceRegistryException {
		String resourceType = TypeUtility.getTypeName(resourceClass);
		String consistsOfType = TypeUtility.getTypeName(consistsOfClass);
		String facetType = TypeUtility.getTypeName(facetClass);
		String ret = getFilteredResources(resourceType, consistsOfType, facetType, polymorphic, facetConstraints);
		try {
			return (List<R>) ElementMapper.unmarshalList(Resource.class, ret);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String getFilteredResources(String resourceType, String consistsOfType, String facetType,
			boolean polymorphic, Map<String,String> facetConstraints) throws ResourceRegistryException {
		return getRelated(resourceType, consistsOfType, facetType, Direction.OUT, polymorphic, facetConstraints);
	}
	
	@Override
	public <R extends Resource, I extends IsRelatedTo<?,?>, RR extends Resource> List<R> getRelatedResourcesFromReferenceResource(
			Class<R> resourceClass, Class<I> isRelatedToClass, RR referenceResource,
			Direction direction, boolean polymorphic) throws ResourceRegistryException {
		UUID referenceResourceUUID = referenceResource.getID();
		@SuppressWarnings("unchecked")
		Class<RR> referenceResourceClass = (Class<RR>) referenceResource.getClass();
		return getRelatedResourcesFromReferenceResource(resourceClass, isRelatedToClass, referenceResourceClass,
				referenceResourceUUID, direction, polymorphic);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <R extends Resource, I extends IsRelatedTo<?,?>, RR extends Resource> List<R> getRelatedResourcesFromReferenceResource(
			Class<R> resourceClass, Class<I> isRelatedToClass, Class<RR> referenceResourceClass,
			UUID referenceResourceUUID, Direction direction, boolean polymorphic) throws ResourceRegistryException {
		String resourceType = TypeUtility.getTypeName(resourceClass);
		String isRelatedToType = TypeUtility.getTypeName(isRelatedToClass);
		String referenceResourceType = TypeUtility.getTypeName(referenceResourceClass);
		String ret = getRelatedResourcesFromReferenceResource(resourceType, isRelatedToType, referenceResourceType,
				referenceResourceUUID, direction, polymorphic);
		try {
			return (List<R>) ElementMapper.unmarshalList(Resource.class, ret);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String getRelatedResourcesFromReferenceResource(String resourceType, String isRelatedToType,
			String referenceResourceType, UUID referenceResourceUUID, Direction direction, boolean polymorphic)
			throws ResourceRegistryException {
		return getRelated(resourceType, isRelatedToType, referenceResourceType, referenceResourceUUID, direction,
				polymorphic);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <R extends Resource, I extends IsRelatedTo<?,?>, RR extends Resource> List<R> getRelatedResources(
			Class<R> resourceClass, Class<I> isRelatedToClass, Class<RR> referenceResourceClass, Direction direction,
			boolean polymorphic) throws ResourceRegistryException {
		String resourceType = TypeUtility.getTypeName(resourceClass);
		String isRelatedToType = TypeUtility.getTypeName(isRelatedToClass);
		String referenceResourceType = TypeUtility.getTypeName(referenceResourceClass);
		String ret = getRelatedResources(resourceType, isRelatedToType, referenceResourceType, direction, polymorphic);
		try {
			return (List<R>) ElementMapper.unmarshalList(Resource.class, ret);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String getRelatedResources(String resourceType, String isRelatedToType, String referenceResourceType,
			Direction direction, boolean polymorphic) throws ResourceRegistryException {
		return getRelated(resourceType, isRelatedToType, referenceResourceType, direction, polymorphic, null);
	}
	
	@SuppressWarnings("unchecked")
	// @Override
	protected <E extends Entity, R extends Relation<?,?>, RE extends Entity> List<E> getRelated(Class<E> entityClass,
			Class<R> relationClass, Class<RE> referenceEntityClass, Direction direction, boolean polymorphic,
			Map<String,String> map) throws ResourceRegistryException {
		String entityType = TypeUtility.getTypeName(entityClass);
		String relationType = TypeUtility.getTypeName(relationClass);
		String referenceEntityType = TypeUtility.getTypeName(referenceEntityClass);
		String ret = getRelated(entityType, relationType, referenceEntityType, direction, polymorphic, map);
		try {
			return (List<E>) ElementMapper.unmarshalList(Resource.class, ret);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	// @Override
	protected String getRelated(String entityType, String relationType, String referenceEntityType, Direction direction,
			boolean polymorphic, Map<String,String> facetConstraints) throws ResourceRegistryException {
		return getRelated(entityType, relationType, referenceEntityType, null, direction, polymorphic, facetConstraints);
	}
	
	// @Override
	protected  <E extends Entity, R extends Relation<?,?>, RE extends Entity> List<E> getRelated(Class<E> entityClass,
			Class<R> relationClass, Class<RE> referenceEntityClass, RE referenceEntity, Direction direction,
			boolean polymorphic) throws ResourceRegistryException {
		UUID referenceEntityUUID = referenceEntity.getID();
		return getRelated(entityClass, relationClass, referenceEntityClass, referenceEntityUUID, direction,
				polymorphic);
	}
	
	@SuppressWarnings("unchecked")
	// @Override
	protected  <E extends Entity, R extends Relation<?,?>, RE extends Entity> List<E> getRelated(Class<E> entityClass,
			Class<R> relationClass, Class<RE> referenceEntityClass, UUID referenceEntityUUID, Direction direction,
			boolean polymorphic) throws ResourceRegistryException {
		String entityType = TypeUtility.getTypeName(entityClass);
		String relationType = TypeUtility.getTypeName(relationClass);
		String referenceEntityType = TypeUtility.getTypeName(referenceEntityClass);
		String ret = getRelated(entityType, relationType, referenceEntityType, referenceEntityUUID, direction,
				polymorphic);
		try {
			return (List<E>) ElementMapper.unmarshalList(Resource.class, ret);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	// @Override
	protected  String getRelated(String entityType, String relationType, String referenceEntityType, UUID referenceEntity,
			Direction direction, boolean polymorphic) throws ResourceRegistryException {
		return getRelated(entityType, relationType, referenceEntityType, referenceEntity, direction, polymorphic, null);
	}

	@Override
	public <ERElem extends ERElement> Map<UUID, String> getInstanceContexts(Class<ERElem> clazz, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		String typeName = TypeUtility.getTypeName(clazz);
		return getInstanceContexts(typeName, uuid);
	}

	@Override
	public Map<UUID, String> getInstanceContexts(String type, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		try {
			logger.trace("Going to get contexts of {} with UUID {}", type, uuid);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(uuid.toString());
			gxHTTPStringRequest.path(AccessPath.CONTEXTS_PATH_PART);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			String objectNode = HTTPUtility.getResponse(String.class, httpURLConnection);

			logger.info("Contexts of {} with UUID {} are {}", type, uuid, objectNode);
			
			Map<UUID, String> contexts = ContextUtility.getContextMap(objectNode);
			return contexts;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while getting {} with UUID {}", type, uuid, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while getting {} with UUID {}", type, uuid, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<QueryTemplate> getAllQueryTemplates() throws ResourceRegistryException {
		try {
			logger.trace("Going to list {}s", QueryTemplate.NAME);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(QueryTemplatePath.QUERY_TEMPLATES_PATH_PART);
			
			Map<String,String> parameters = new HashMap<>();
			addIncludeMeta(parameters);
			gxHTTPStringRequest.queryParams(parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			String all = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.debug("Got {}s are {}", QueryTemplate.NAME, all);
			
			JavaType type = ElementMapper.getObjectMapper().getTypeFactory().constructCollectionType(ArrayList.class, QueryTemplate.class);
			return ElementMapper.getObjectMapper().readValue(all, type);
			
		} catch(ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean existQueryTemplate(QueryTemplate queryTemplate)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		return existQueryTemplate(queryTemplate.getName());
	}

	@Override
	public boolean existQueryTemplate(String queryTemplateName)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		try {
			logger.trace("Going to read {} with name {}", QueryTemplate.NAME, queryTemplateName);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(QueryTemplatePath.QUERY_TEMPLATES_PATH_PART);
			gxHTTPStringRequest.path(queryTemplateName);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.head();
			HTTPUtility.getResponse(String.class, httpURLConnection);
			return true;
		} catch (NotFoundException e) {
			return false;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public QueryTemplate readQueryTemplate(QueryTemplate queryTemplate)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		return readQueryTemplate(queryTemplate.getName());
	}

	@Override
	public QueryTemplate readQueryTemplate(String queryTemplateName)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		try {
			String queryTemplate = readQueryTemplateAsString(queryTemplateName);
			return ElementMapper.unmarshal(QueryTemplate.class, queryTemplate);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String readQueryTemplateAsString(String queryTemplateName)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		try {
			logger.trace("Going to read {} with name {}", QueryTemplate.NAME, queryTemplateName);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(QueryTemplatePath.QUERY_TEMPLATES_PATH_PART);
			gxHTTPStringRequest.path(queryTemplateName);
			
			Map<String,String> parameters = new HashMap<>();
			addIncludeMeta(parameters);
			gxHTTPStringRequest.queryParams(parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			String c = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.debug("Got {} is {}", QueryTemplate.NAME, c);
			return c;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public String runQueryTemplateGetString(String name)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		return runQueryTemplate(name, "");
	}

//	@Override
//	public <E extends Entity> List<E> runQueryTemplate(String name)
//			throws QueryTemplateNotFoundException, ResourceRegistryException {
//		try {
//			String ret = runQueryTemplateGetString(name);
//			JavaType type = ElementMapper.getObjectMapper().getTypeFactory().constructCollectionType(ArrayList.class, Entity.class);
//			return ElementMapper.getObjectMapper().readValue(ret, type);
//		} catch(ResourceRegistryException e) {
//			throw e;
//		} catch(Exception e) {
//			throw new RuntimeException(e);
//		}
//	}

	@Override
	public String runQueryTemplate(String name)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		try {
			String ret = runQueryTemplateGetString(name);
			return ret;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String runQueryTemplate(QueryTemplate queryTemplate)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		return runQueryTemplate(queryTemplate.getName());
	}

	@Override
	public String runQueryTemplate(String name, String params)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		try {
			if(params==null || params.compareTo("")==0) {
				logger.trace("Going to run {} using default parameters", QueryTemplate.NAME);
				params = null;
			}else {
				logger.trace("Going to run {} with the following parameters {}", QueryTemplate.NAME, params);
			}
			
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.header(CONTENT_TYPE_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(QueryTemplatePath.QUERY_TEMPLATES_PATH_PART);
			gxHTTPStringRequest.path(name);
			
			includeAdditionalQueryParameters(gxHTTPStringRequest);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.post(params);
			String c = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.trace("The result of the query is {}", c);
			return c;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String runQueryTemplate(String name, JsonNode jsonNode)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String ret = runQueryTemplate(name, objectMapper.writeValueAsString(jsonNode));
			return ret;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String runQueryTemplate(QueryTemplate queryTemplate, JsonNode jsonNode)
			throws QueryTemplateNotFoundException, ResourceRegistryException {
		return runQueryTemplate(queryTemplate.getName(), jsonNode);
	}

	@Override
	public String jsonQuery(String query) throws InvalidQueryException, ResourceRegistryException {
		try {
			logger.trace("Going to run the following JSON Query {}", query);
			
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.header(CONTENT_TYPE_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.QUERY_PATH_PART);
			
			includeAdditionalQueryParameters(gxHTTPStringRequest);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.post(query);
			String c = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.trace("The result of the query is {}", c);
			return c;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public <E extends Entity> List<E> jsonQuery(JsonNode jsonNode)
			throws InvalidQueryException, ResourceRegistryException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String ret = jsonQuery(objectMapper.writeValueAsString(jsonNode));
			JavaType type = ElementMapper.getObjectMapper().getTypeFactory().constructCollectionType(ArrayList.class, Entity.class);
			return ElementMapper.getObjectMapper().readValue(ret, type);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
