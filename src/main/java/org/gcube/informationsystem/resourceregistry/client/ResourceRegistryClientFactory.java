package org.gcube.informationsystem.resourceregistry.client;

import org.gcube.informationsystem.resourceregistry.api.Constants;
import org.gcube.informationsystem.resourceregistry.api.rest.ServiceInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceRegistryClientFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceRegistryClientFactory.class);
	
	public static String getResourceRegistryURL() {
		String address = String.format("%s/%s", ServiceInstance.getServiceURL(),Constants.SERVICE_NAME);
		return address;
	}
	
	public static String getResourceRegistryURL(String context) {
		String address = String.format("%s/%s", ServiceInstance.getServiceURL(context),Constants.SERVICE_NAME);
		return address;
	}
	
	public static ResourceRegistryClient create() {
		String address = getResourceRegistryURL();
		logger.trace("The {} will be contacted at {}", Constants.SERVICE_NAME, address);
		return new ResourceRegistryClientImpl(address);
	}
	
	public static ResourceRegistryClient create(String context) {
		String address = getResourceRegistryURL(context);
		logger.trace("The {} will be contacted at {}", Constants.SERVICE_NAME, address);
		return new ResourceRegistryClientImpl(address);
	}
	
}
