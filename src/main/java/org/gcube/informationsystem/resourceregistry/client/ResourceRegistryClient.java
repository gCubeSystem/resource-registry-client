package org.gcube.informationsystem.resourceregistry.client;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.Direction;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.knowledge.ModelKnowledge;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates.QueryTemplateNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.request.RequestInfo;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.types.knowledge.TypeInformation;
import org.gcube.informationsystem.types.reference.Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface ResourceRegistryClient extends RequestInfo {

	/**
	 * Use {@link #includeContexts()} instead
	 * @return
	 */
	@Deprecated
	public boolean isIncludeContextsInHeader();

	/**
	 * Use {@link #includeContexts(boolean)} instead
	 * @param includeContextsInHeader
	 */
	@Deprecated
	public void setIncludeContextsInHeader(boolean includeContexts);
	
	public void addHeader(String name, String value);
	
	
	public List<Context> getAllContext() throws ResourceRegistryException;
	
	public ContextCache getContextCache();
	
	public boolean existContext(String uuid) throws ContextNotFoundException, ResourceRegistryException;

	public boolean existContext(UUID uuid) throws ContextNotFoundException, ResourceRegistryException;

	public Context getContext(String uuid) throws ContextNotFoundException, ResourceRegistryException;

	public Context getContext(UUID uuid) throws ContextNotFoundException, ResourceRegistryException;

	public Context getCurrentContext() throws ContextNotFoundException, ResourceRegistryException;

	/* ---------------------------------------------------------------------- */

	public ModelKnowledge<Type, TypeInformation> getModelKnowledge();
	
	public void renewModelKnowledge();
	
	public boolean existType(String typeName) throws ResourceRegistryException;
	
	public <ME extends ModelElement> boolean existType(Class<ME> clazz) throws ResourceRegistryException;

	public String getType(String typeName, Boolean polymorphic)
			throws SchemaNotFoundException, ResourceRegistryException;

	public String getType(String typeName, int level)
			throws SchemaNotFoundException, ResourceRegistryException;
	
	public <ME extends ModelElement> List<Type> getType(Class<ME> clazz, Boolean polymorphic)
			throws SchemaNotFoundException, ResourceRegistryException;
	
	public <ME extends ModelElement> List<Type> getType(Class<ME> clazz, int level)
			throws SchemaNotFoundException, ResourceRegistryException;
	
	public <ME extends ModelElement> Node<Type> getTypeTreeNode(Class<ME> clazz)
			throws SchemaNotFoundException, ResourceRegistryException;
	
	public Node<Type> getTypeTreeNode(String typeName)
			throws SchemaNotFoundException, ResourceRegistryException;
	
	/* ---------------------------------------------------------------------- */

	public <ERElem extends ERElement> List<ERElem> getInstances(Class<ERElem> clazz, Boolean polymorphic)
			throws ResourceRegistryException;

	public String getInstances(String type, Boolean polymorphic) throws ResourceRegistryException;

	public <ERElem extends ERElement> boolean existInstance(Class<ERElem> clazz, UUID uuid)
			throws AvailableInAnotherContextException, ResourceRegistryException;

	public boolean existInstance(String type, UUID uuid)
			throws AvailableInAnotherContextException, ResourceRegistryException;

	public <ERElem extends ERElement> ERElem getInstance(Class<ERElem> clazz, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException;

	public String getInstance(String type, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException;

	/* ---------------------------------------------------------------------- */

	public <ERElem extends ERElement> Map<UUID, String> getInstanceContexts(Class<ERElem> clazz, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException;

	public Map<UUID, String> getInstanceContexts(String type, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException;

	/* ---------------------------------------------------------------------- */

	public List<QueryTemplate> getAllQueryTemplates() throws ResourceRegistryException;

	public boolean existQueryTemplate(QueryTemplate queryTemplate) throws ResourceRegistryException;

	public boolean existQueryTemplate(String queryTemplateName) throws ResourceRegistryException;

	public QueryTemplate readQueryTemplate(QueryTemplate queryTemplate)
			throws QueryTemplateNotFoundException, ResourceRegistryException;

	public QueryTemplate readQueryTemplate(String queryTemplateName)
			throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String readQueryTemplateAsString(String queryTemplateName)
			throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String runQueryTemplateGetString(String name)
			throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String runQueryTemplate(String name)
			throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String runQueryTemplate(QueryTemplate queryTemplate)
			throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String runQueryTemplate(String name, String params)
			throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String runQueryTemplate(String name, JsonNode jsonNode)
			throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String runQueryTemplate(QueryTemplate queryTemplate, JsonNode jsonNode)
			throws QueryTemplateNotFoundException, ResourceRegistryException;

	/* ---------------------------------------------------------------------- */

	public <R extends Resource, C extends ConsistsOf<?, ?>, F extends Facet> List<R> getResourcesFromReferenceFacet(
			Class<R> resourceClass, Class<C> consistsOfClass, F referenceFacet, boolean polymorphic)
			throws ResourceRegistryException;

	public <R extends Resource, C extends ConsistsOf<?, ?>, F extends Facet> List<R> getResourcesFromReferenceFacet(
			Class<R> resourceClass, Class<C> consistsOfClass, Class<F> facetClass, UUID referenceFacetUUID,
			boolean polymorphic) throws ResourceRegistryException;

	public String getResourcesFromReferenceFacet(String resourceType, String consistsOfType, String facetType,
			UUID referenceFacetUUID, boolean polymorphic) throws ResourceRegistryException;

	public <R extends Resource, C extends ConsistsOf<?, ?>, F extends Facet> List<R> getFilteredResources(
			Class<R> resourceClass, Class<C> consistsOfClass, Class<F> facetClass, boolean polymorphic,
			Map<String, String> facetConstraint) throws ResourceRegistryException;

	public String getFilteredResources(String resourceType, String consistsOfType, String facetType,
			boolean polymorphic, Map<String, String> facetConstraint) throws ResourceRegistryException;

	public <R extends Resource, I extends IsRelatedTo<?, ?>, RR extends Resource> List<R> getRelatedResourcesFromReferenceResource(
			Class<R> resourceClass, Class<I> isRelatedToClass, RR referenceResource, Direction direction,
			boolean polymorphic) throws ResourceRegistryException;

	public <R extends Resource, I extends IsRelatedTo<?, ?>, RR extends Resource> List<R> getRelatedResourcesFromReferenceResource(
			Class<R> resourceClass, Class<I> isRelatedToClass, Class<RR> referenceResourceClass,
			UUID referenceResourceUUID, Direction direction, boolean polymorphic) throws ResourceRegistryException;

	public String getRelatedResourcesFromReferenceResource(String resourceType, String isRelatedToType,
			String referenceResourceType, UUID referenceResourceUUID, Direction direction, boolean polymorphic)
			throws ResourceRegistryException;

	public <R extends Resource, I extends IsRelatedTo<?, ?>, RR extends Resource> List<R> getRelatedResources(
			Class<R> resourceClass, Class<I> isRelatedToClass, Class<RR> referenceResourceClass, Direction direction,
			boolean polymorphic) throws ResourceRegistryException;

	public String getRelatedResources(String resourceType, String isRelatedToType, String referenceResourceType,
			Direction direction, boolean polymorphic) throws ResourceRegistryException;

	/* ---------------------------------------------------------------------- */

	public String jsonQuery(final String query)
			throws InvalidQueryException, ResourceRegistryException;

	public <E extends Entity> List<E> jsonQuery(final JsonNode jsonNode)
			throws InvalidQueryException, ResourceRegistryException;
	
	/* ---------------------------------------------------------------------- */
	
	public String rawQuery(final String query)
			throws InvalidQueryException, ResourceRegistryException;

	public String rawQuery(final String query, boolean raw)
			throws InvalidQueryException, ResourceRegistryException;

}
