This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry Client

## [v4.5.0-SNAPSHOT]

- Added getContextCache() to be able to get Context as Tree [#24555]
- Added support for model knowledge [#25922]
- Added support for paginated results [#24648]


## [v4.4.0]

- Migrated code to reorganized E/R format [#24992]


## [v4.3.0]

- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context


## [v4.2.0]

- Aligned APIs to other clients [#22011]
- Moved Direction class in information-system-model
- Added support for context names included in header among UUIDs [#22090]
- Added JSON Query API [#22047][#22815]
- Added QueryTemplate safe APIs [#22091][#22815]
- Client gets service URL using resource-registry-api lib utility [#23658]


## [v4.1.0] 

- Used ContextCache to make the client more efficient
- Added APIs to get instance contexts [#20013]
- Added support to request contexts in instances header [#20012]
 

## [v4.0.0] [r4.26.0] - 2020-11-11 

- Switched JSON management to gcube-jackson [#19116]


## [v3.0.0] [r4.21.0] - 2020-03-30

- Refactored code to support IS Model reorganization (e.g naming, packages)
- Using gxREST in place of custom class of resource-registry-api [#11455]
- Refactored code to support renaming of Embedded class to Property [#13274]


## [v2.0.0] [r4.13.0] - 2018-11-20

- Using new resource-registry REST interface [#11902]


## [v1.5.0] [r4.9.0] - 2017-12-20

- Removed API which need to pass the java class to specify the type [#10318]
- Added APis having a string as input containing serialization of entity or relation instead of the class instances [#9877]
- Changed pom.xml to use new make-servicearchive directive [#10165]
- Added API to read Context definition [#10246]
- Added API to get all Context definition [#10610]
- Added API getFilteredResources() [#10172]


## [v1.4.0] [r4.6.0] - 2017-07-25

- Added HAProxy discovery support [#8757]


## [v1.3.0] [r4.5.0] - 2017-06-07

- Removed common-generic-clients, common-gcube-calls and common-fw-clients from dependencies
- The client is not implement as gcube-proxy anymore. It is now a generic http-client (using HTTPCall class provided in API) autoquery the deprecated IS to get resource-registry instance.
- Added exists() method which uses HEAD HTTP method to get existence from Resource Registry


## [v1.2.0] [r4.3.0] - 2017-03-16

- Added API to support Entity-Relation navigation
- Added API to get the list of ER types and their own schema
- Added ResourceRegistryException JSON deserialization


## [v1.1.0] [r4.2.0] - 2016-12-15

- Improved code quality


## [v1.0.0] [r4.1.0] - 2016-11-07

- First Release

